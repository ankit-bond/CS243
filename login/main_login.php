 <script>
        window.fbAsyncInit = function() {
                FB.init({
                appId: '181347822253454',
                status: true,
                cookie: true,
                xfbml: true
            });
        };
        (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
        }(document));

        function login() {
            FB.login(function(response) {
                var userID=response.authResponse.userID;
                PARS(userID);
            }, {scope: 'public_profile,email'});            
        }
        var status = FB.getLoginStatus();
        function PARS(userID) {
        FB.api('/me', function(response) {
        var name=response.name;
        document.location='login/fb.php?id='+userID+'&name='+name;

      });
    }
    </script>
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Here</h4>
            </div>
            <div class="modal-body">
                <form method="POST" action="login/checklogin.php">
                    <input type="hidden" name="_csrf">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="username" required >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="password" required>
                    </div>
                     <button onclick="javascript:login();" class="btn btn-primary" style="width:300px; margin-left:50px; " >Login using Facebook</button>
                        <p>Not Registered Yet? <a href="register/main_registration.php">Register Here</a><p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button class="btn btn-success" type="submit" id="submit">Login</button>
            </div>
            </form>
        </div>
    </div>
</div>