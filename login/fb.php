<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"]=="GET") {
		$userID = $_GET['id'];
		$name=$_GET['name'];
		$_SESSION['username']=$userID;
		$_SESSION['fullname']=$name;
        $_SESSION['fb_login']=true;
		include("../dbconnect.php");
		$sql = "select * from $tbl_name where username = '$userID'";
		$result=mysqli_query($link,$sql);
		$row=mysqli_num_rows($result);
		if($row==1){
			header("location:../account/dashboard.php");
		}
	}
?>
<?php 
	if($row!=1):?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>fb login</title>
	<link href="../css/bootstrap.css" rel="stylesheet">
    
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
</head>
<body>
	<div class="container col-lg-offset-3 col-md-offset-2 col-sm-12 col-lg-6 col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Fill the Details</h3>
            <span class="header_text" >you have been successfully logged in using your FB account</span>
        </div>
        <form class="form-horizontal" action="../register/fb_registration.php" method="POST">
        <div class="panel-body">
                <div class="form-group">
                    <label for="email" class="control-label col-md-4 col-sm-12">Email Address</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="email" name="email" id="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone-number" class="control-label col-md-4 col-sm-12">Phone Number</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="number" name="phone_number" id="phone-number" placeholder="Mobile Number" required>
                    </div>
                </div>
                <div align="center">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="type" value="patient" checked>Patient
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="type" value="doctor">Doctor
                        </label>
                    </div>
                </div>
            <br>
            <div class="col-md-8 col-lg-8 col-sm-12 pull-right">
                <button type="submit" class="btn btn-success btn-block pull-right"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;&nbsp;Submit</button>
            </div>
            </div>
    	</form>
	</div>
</div>
</body>
</html>
<?php endif;?>