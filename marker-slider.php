<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder = new google.maps.Geocoder();
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}
function updateMarkerPosition(latLng) {
  var latitude=latLng.lat();
  var longitude=latLng.lng();
}

function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
  var address=str;
}

function Mapshow() {
  var latLng = new google.maps.LatLng(26.316082,91.598396);
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Current location',
    map: map,
    draggable: true
  });
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  google.maps.event.addListener(marker, 'dragstart');
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
}
google.maps.event.addDomListener(window, 'load', Mapshow);
</script>
<link rel="stylesheet" type="text/css" href="css/upload.css">
</head>
<body>
  <div id="mapCanvas"></div>
  <b>Matching address:</b>
  <div id="address"></div>
</body>
</html>