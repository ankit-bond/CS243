<html>
<head>
	<title>NEW POST</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/forumstyle.css">
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="../">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="../forum/forum_main.php">Forum</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div>
</div>
<?php
session_start();
    require"../db_connect.php";
    if(isset($_POST['topic_name']) && !empty($_POST['topic_name']) && isset($_POST['topic_content']) && !empty($_POST['topic_content']))
    {
        $topic_name=$_POST['topic_name'];
        $topic_content=$_POST['topic_content'];
        $t=time();
        $post_time=date("h:i a");
        $post_date=date("jS \ F");


        if(isset($_SESSION['username']))
        {
            $post_creator=$_SESSION['username'];
            $creator_email="";
        }
        else
        {
            if(isset($_POST['post_creator']) && !empty($_POST['post_creator']) && isset($_POST['creator_email']) && !empty($_POST['creator_email']))
            {
                $post_creator=$_POST['post_creator'];
                $creator_email=$_POST['creator_email'];
            }
        }
        $sql2 = "INSERT INTO forum(forum_name,forum_content,post_creator,creator_email,post_time,post_date) VALUES('$topic_name','$topic_content','$post_creator','$creator_email','$post_time','$post_date')";
        if ($db->query($sql2) === True) 
        {
            header('Location: ../forum/forum_main.php');
        }
        else
        {
            echo 'Topic not Created.';
        }
    }
    else
    {
            
    }
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8" style="background-color:#ededed;">
            <div>
                <h3>Start a new Discussion Here.</h3>
                <form  method="post" >
                    Enter Name of New Topic.<br>
                    <input type="text" name="topic_name" size="30" id="fname"><br>
                    Enter Your Post.<br>
                    <textarea name="topic_content" id="reply" placeholder="Enter Your Post Here...."></textarea><br><br>
                    <?php
                        if(!isset($_SESSION['username'])):
                    ?>
                    Enter Your Name<br>
                    <input type="text" name="post_creator" size="30" id="fname"><br>
                    Enter Your Emal-id<br>
                    <input type="text" name="creator_email" size="30" id="fname"><br>
                    <?php
                        endif;
                    ?>
                    <button type="submit" name="submit" class="btn btn-success">Submit</button><br><br><br>
                </form>
            </div>
        </div>
        <div class="col-sm-2">
        </div>
    </div>
</div>
<script>
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            var userID=response.authResponse.userID;
            PARS(userID);
        } else if (response.status === 'not_authorized') {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into this app.';
        } else {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into Facebook.';
        }
    }
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '181347822253454',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.5'
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    function PARS(userID) {
        FB.api('/me', function(response) {
            var name=response.name;
            document.location='../login/fb.php?id='+userID+'&name='+name;

        });
    }
</script>
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Here</h4>
            </div>
            <form method="POST" action="../login/checklogin.php">
            <div class="modal-body">
                    <input type="hidden" name="_csrf">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="username" required >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="password" required>
                    </div>
                    <span class="fb-button" >Or Login using &nbsp;&nbsp;
                        <fb:login-button scope="public_profile,email" onclick="checkLoginState(); ">
                        </fb:login-button>
                    </span>
                    <p>Not Registered Yet? <a href="../register/main_registration.php">Register Here</a><p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button class="btn btn-success" type="submit" id="submit">Login</button>
            </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>