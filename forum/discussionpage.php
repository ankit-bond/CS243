<?php
    require "../db_connect.php";
    session_start();
    $forum_id=$_GET['id'];
    if(isset($_POST['reply_text']) && !empty($_POST['reply_text']))
    {
        $reply=$_POST['reply_text'];
        if(isset($_SESSION['username']))
        {
            $user_name=$_SESSION['username'];
            $sql2="INSERT INTO forum_post(post_author,post_body,forum_id) VALUES('$user_name','$reply','$forum_id')";
        }
        else
        {
            if(isset($_POST['user_name']) && !empty($_POST['user_name']))
            {
                $user_name=$_POST['user_name'];
                $sql2="INSERT INTO forum_post(post_author,post_body,forum_id) VALUES('$user_name','$reply','$forum_id')";
            }
            else
            {
                echo 'Please Enter Your Details.';
            }
        }
        if($query2=$db->prepare($sql2))
        {
            $query2->execute();
        }
    }
    $sql3 = "SELECT forum_name, forum_content, post_creator, post_time, post_date FROM forum WHERE forum_id='$forum_id'";
    if($query=$db->prepare($sql3))
        {
            $query->bind_result($f_name,$f_content,$f_creator,$post_time,$post_date);
            $query->execute();
            $query->store_result();
        }
        else
        {
            echo $db->error;
        }
        while ($row=$query->fetch())
        {
            $name= $f_name;
            $content= $f_content;
            $creator= $f_creator;
            $date=$post_date;
            $time=$post_time;   
        }
?>
<html>
<head>
	<title>DiscussionPage</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/forumstyle.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="../">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="forum_main.php">Back to Forums<span class="sr-only">(current)</span></a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-7">
            <h3>Discussion Page</h3>
            <div id="forumcontent">
                <h3>
                    <?php
                        echo $name;
                    ?>
                </h3>
                By
                <i>
                <?php
                    echo $creator;
                ?>
                </i>
                <div id="datepadding">
                    At
                    <?php 
                        echo $time." ".$date;
                    ?>
                </div>
                <hr id="style2">
                <?php
                    echo $content;
                ?>  
                <hr id="style2"> 
            </div><br>
            <hr id="style1">
            <h3>Comments</h3>
            <br>
            <?php 
                $sql = "SELECT post_author, post_body FROM forum_post WHERE forum_id='$forum_id'";
                if($query=$db->prepare($sql))
                {
                    $query->bind_result($f_author,$f_body);
                    $query->execute();
                    $query->store_result();
                }
                else
                {
                    echo $db->error;
                }
                if($query->num_rows !==0):
                while($row= $query->fetch()):
            ?>
                <div class="panel panel-info">
                            <div class="panel-body">
                                <font size="4">
                                    <?php echo $f_body;  ?>
                                </font><br>
                                By <?php echo $f_author; ?>
                            </div>
                </div>
            <?php endwhile; endif;?>
            <hr id="style1">
            <div id="formstyle">
            <form method="post" class="form">
                <h3>Reply</h3>
                Your Email will not be disclosed.
                <hr id="style2">
                <?php
                    if(!isset($_SESSION['username'])):
                ?>
                Enter Your Name<br>
                <input type="text" name="user_name" size="30" id="fname"><br>
                Enter Your Emal-id<br>
                <input type="text" name="user_emailid" size="30" id="email"><br>
                <?php
                    endif;
                ?>
                Enter Your Reply<br>
                <textarea name="reply_text" id="reply" placeholder="Enter Your Comment Here...."></textarea><br><br>
                <button type="submit" name="reply" class="btn btn-success">Submit</button>
            </form>
        </div>
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-3">
            <a class="btn btn-primary" href="../forum/new_post.php" role="button">
                <span class="glyphicon glyphicon-pencil"></span>
                New Post</a>
        </div>
    </div>
</div>
<script>
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            var userID=response.authResponse.userID;
            PARS(userID);
        } else if (response.status === 'not_authorized') {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into this app.';
        } else {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into Facebook.';
        }
    }
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '181347822253454',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.5'
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    function PARS(userID) {
        FB.api('/me', function(response) {
            var name=response.name;
            document.location='../login/fb.php?id='+userID+'&name='+name;

        });
    }
</script>
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Here</h4>
            </div>
            <form method="POST" action="../login/checklogin.php">
                <div class="modal-body">
                    <input type="hidden" name="_csrf">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="username" required >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="password" required>
                    </div>
                    <span class="fb-button" >Or Login using &nbsp;&nbsp;
                        <fb:login-button scope="public_profile,email" onclick="checkLoginState(); ">
                        </fb:login-button>
                    </span>
                    <p>Not Registered Yet? <a href="../register/main_registration.php">Register Here</a><p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit" id="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>