<html>
<head>
	<title>Forum</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/forumstyle.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="../">Home<span class="sr-only">(current)</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<?php
session_start();
    require"../db_connect.php";
    $sql = "SELECT forum_id, forum_name, forum_content, post_creator, post_time, post_date FROM forum";
    if($query=$db->prepare($sql))
    {
        $query->bind_result($f_id,$f_name,$f_content,$post_creator,$post_time,$post_date);
        $query->execute();
        $query->store_result();
    }
    else
    {
        echo $db->error;
    }
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-1">
        </div>
        <div class="col-sm-7" style="background-color:#ededed;">
            <h1>FORUM</h1>
            <div>
                <div id="container-fluid">
                    <?php 
                        if($query->num_rows !==0):
                        while($row= $query->fetch()):
                    ?>
                    <div class="panel panel-info">
                        <div class="panel-body">
                            <font size="5"><a href="../forum/discussionpage.php?id=<?php echo $f_id; ?>">
                                <?php echo $f_name;  ?>
                            </a></font><br>
                            By <i><?php echo $post_creator; ?></i><br>
                            At <?php echo $post_time." ".$post_date ?>
                        </div>
                    </div>
                    <?php endwhile; endif;  ?>
                </div>
            </div>
        </div>
        <div class="col-sm-1">
        </div>
        <div class="col-sm-3">
            <a class="btn btn-primary" href="../forum/new_post.php" role="button">
                <span class="glyphicon glyphicon-pencil"></span>
                New Post</a>
        </div>
    </div>
</div>
<script>
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            var userID=response.authResponse.userID;
            PARS(userID);
        } else if (response.status === 'not_authorized') {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into this app.';
        } else {
            //document.getElementById('status').innerHTML = 'Please log ' +
            //'into Facebook.';
        }
    }
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '181347822253454',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.5'
        });

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    function PARS(userID) {
        FB.api('/me', function(response) {
            var name=response.name;
            document.location='../login/fb.php?id='+userID+'&name='+name;

        });
    }
</script>
<div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Login Here</h4>
            </div>
            <form method="POST" action="../login/checklogin.php">
                <div class="modal-body">
                    <input type="hidden" name="_csrf">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" id="username" placeholder="username" required >
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" name="password" id="password" placeholder="password" required>
                    </div>
                    <span class="fb-button" >Or Login using &nbsp;&nbsp;
                        <fb:login-button scope="public_profile,email" onclick="checkLoginState(); ">
                        </fb:login-button>
                    </span>
                    <p>Not Registered Yet? <a href="../register/main_registration.php">Register Here</a><p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button class="btn btn-success" type="submit" id="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>