#!/usr/bin/python
from simplecrypt import decrypt
import sys

password = sys.argv[2]
filename = sys.argv[1]

def main():
	try:
		ciphertext = read_file(filename)
		text = decrypt_data(password,ciphertext)
		write_file(filename,text)
		print(1)
	except Exception, e:
		print(0)

        
def read_file(filename):
    with open(filename, 'rb') as input:
        ciphertext = input.read()
        return ciphertext

def decrypt_data(password, data):
    text = decrypt(password, data)
    return text


def write_file(filename, data):
    with open(filename, 'wb') as output:
        output.write(data)

if __name__ == '__main__':
	main()