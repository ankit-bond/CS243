#!/usr/bin/python

from simplecrypt import encrypt
import sys

password = sys.argv[2]
filename = sys.argv[1]
def main():
	print("reading...")
	text = read_file(filename)
	print("read %s from %s\n" % (text, filename))
	ciphertext = encrypt_data(password,text)
	print("encrypted text %s\n" % (ciphertext))
	write_file(filename,ciphertext)
	print("file written\n")

def read_file(filename):
    with open(filename, 'rb') as input:
        text = input.read()
        return text

def encrypt_data(password, data):
    ciphertext = encrypt(password, data)
    return ciphertext


def write_file(filename, data):
    with open(filename, 'wb') as output:
        output.write(data)

if __name__ == '__main__':
	main()