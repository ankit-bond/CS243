$(function()
	{
		$(document).on('submit','#chatForm',function(){
			var text = $.trim($("#text").val());
			var sender = $.trim($("#sender").val());
			var receiver = $.trim($("#receiver").val());
			if(text !="" && sender !="" && receiver !="")
			{
				$.post('chatposter.php',{text: text, sender: sender, receiver: receiver},function(data)
				{
					$(".chatMessages").append(data);
				});
			}
			else
			{
				alert("Data Missing.");
			}
		});

		function getChat(){
			$.get('getdata.php',function(data){
				$(".chatMessages").html(data);
			});
		}

		setInterval(function(){
				getChat();
		},500);
});