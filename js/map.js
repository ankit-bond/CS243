window.onload= function(){
codeLatLng();
var geocoder = new google.maps.Geocoder();
function codeLatLng() {
	var pos;
	var longitude;
	var latitude;
	var option={
		maximumAge:60*1000,
		timeout:5*1000,
		enableHighAccuracy:true,
		zoom:15

	};
	var getLocation = function(position) {
		pos=position;
		latitude=pos.coords.latitude;
		longitude=pos.coords.longitude;
		getPlace(longitude,latitude);
		mapShow(longitude,latitude);
	};
	function mapShow(longitude,latitude) {
		var mapProp = {
  		center:new google.maps.LatLng(latitude,longitude),
  		zoom: 7,
  		mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map=new google.maps.Map(document.getElementById("map"), mapProp);
		var marker=new google.maps.Marker({
  			position:new google.maps.LatLng(latitude,longitude),
  		});
  		marker.setMap(map);
	}

	var err=function(error){
	};
	navigator.geolocation.getCurrentPosition(getLocation,err,option);
    function getPlace(longitude,latitude){
    var latlng = new google.maps.LatLng(latitude, longitude);
    geocoder.geocode({
        'latLng': latlng
    }, function(results, status) {
        var place=results[0].formatted_address;
		document.getElementById('googleaddress').innerHTML = place;
    });
	}
}
}