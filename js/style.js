$(document).ready(function(){
    $("li.nav-dropdown").hover(function() {
        $(this).addClass("open");
    }, function(){
        $(this).removeClass("open");
    });

    $('#LoginModal').on('shown.bs.modal', function () {
        $('#myInput').focus();
    });

    $('#link-profile').click(function(){
        $('#profile').show();
        $('#booked-appointments').hide();
        $('#book-an-appointment').hide();
        $('#update-info').hide();
        $('#reports').hide();
        $('#set-appointment').hide();
        $('#view-patients').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');
    });
    $('#link-book-an-appointment').click(function(){
        $('#book-an-appointment').show();
        $('#booked-appointments').hide();
        $('#profile').hide();
        $('#view-patients').hide();
        $('#update-info').hide();
        $('#reports').hide();
        $('#set-appointment').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');

    });
    $('#link-booked-appointments').click(function(){
        $('#booked-appointments').show();
        $('#profile').hide();
        $('#view-patients').hide();
        $('#book-an-appointment').hide();
        $('#update-info').hide();
        $('#reports').hide();
        $('#set-appointment').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');

    });
    $('#link-update-info').click(function(){
        $('#update-info').show();
        $('#booked-appointments').hide();
        $('#book-an-appointment').hide();
        $('#profile').hide();
        $('#view-patients').hide();
        $('#reports').hide();
        $('#set-appointment').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');

    });

    $('#link-reports').click(function(){
        $('#reports').show();
        $('#booked-appointments').hide();
        $('#book-an-appointment').hide();
        $('#profile').hide();
        $('#view-patients').hide();
        $('#update-info').hide();
        $('#set-appointment').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');
    });
    $('#link-set-appointment').click(function(){
        $('#set-appointment').show();
        $('#reports').hide();
        $('#booked-appointments').hide();
        $('#book-an-appointment').hide();
        $('#profile').hide();
        $('#view-patients').hide();
        $('#update-info').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');
    });
    $('#link-view-patients').click(function(){
        $('#set-appointment').hide();
        $('#reports').hide();
        $('#booked-appointments').hide();
        $('#book-an-appointment').hide();
        $('#profile').hide();
        $('#view-patients').show();
        $('#update-info').hide();
        $('ul.nav-sidebar li').removeClass('active');
        $(this).parent().addClass('active');
    });

});