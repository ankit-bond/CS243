<div id="reports">
<div id="upload-report"  >
    <?php
    if(isset($_GET['reports']))
        if($_GET['reports']==1){
            if(isset($_GET['msg'])){
                if($_GET['msg']==1){
                    echo '<p class="text-info">Only file types allowed: DOCX, DOC, PDF, JPG, JPEG, PNG</p>';
                }
                elseif($_GET['msg']==2){
                    echo '<p class="text-info">File Size exceeded limit.</p>';
                }
            }
        }
    ?>
    <h3 style="margin-left:100px;">Upload Reports</h3>
    <form action="upload.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="files">
    <div class="form-group">
        <input type="file" name="files" class="input-lg form-control" required>
    </div>
    <div class="form-group" style="bottom: -4px;" >
        <input type="submit" name="upload" value="upload" class="input-lg form-control upload_button" >
    </div>
    </form>
</div>
    <div class="container" id="uploaded-report" >
        <div class="col-xs-12">
            <div class="col-sm-6 col-sm-offset-3">
                <h3 class="text-center">Uploaded Reports</a></h3>
                    <?php
                    $sql11 = "select * from $tbl_name5 where pid = $id;";
                    $result11=mysqli_query($link,$sql11);
                    $x=1;
                    if(mysqli_num_rows($result11)==0)
                    {
                        echo '<p class="text-capitalize">You have uploaded no files.</p>';
                    }
                    else{

                        $scriptmodals = '';
                        $modalshtm = '';
                        echo '
                            <table class="table-responsive table table-bordered table-hover">
                            <thead>
                                <tr><th>S.No.</th><th>File Name</th><th>File Type</th><th>Size</th></tr>
                              </thead><tbody>';
                        while($row11=mysqli_fetch_assoc($result11)){
                            $rowfilesize = round($row11['size']/1024,2);
                            echo '<tr><td>'.$x.'</td><td><a href="uploads/'.$row11['filename'].'">'.$row11['filename'].'</a></td><td>'.$row11['filetype'].'</td><td>'.$rowfilesize.'KB</td><td><a class="btn btn-primary" data-toggle="modal" data-target="#LoginModal-'.$x.'">Download</a></td></tr>';
                            $modalshtm .= '<div class="modal fade" id="LoginModal-'.$x.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-'.$x.'">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-sm">
         <form class="form form-inline" method="POST" action="../encryption/decrypt.php?fid='.$row11['id'].'">
            <div class="modal-body">
                <div class="form-group">
                    <label for="password">Password:  </label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" type="submit"">Download</button>
            </div>
            </form>
        </div>
    </div>
</div>';
                            $scriptmodals .= "
    $('#LoginModal-".$x."').on('shown.bs.modal', function () {
    });";
                            $x++;
                        }
                        echo '</tbody></table>';
                        echo $modalshtm;
                    }
                    ?>

            </div>
        </div>
    </div>
</div>
</div>
<script>
<?php
    echo $scriptmodals;
?>
</script>
