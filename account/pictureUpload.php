<?php
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
include("../dbconnect.php");
session_start();
if(!isset($_SESSION["username"]) and !isset($_SESSION["id"])){
	header(" location: ../main_login.php");
}
$extension=array("jpg","jpeg","png");
$id=$_SESSION['id'];
if(isset($_FILES['files']))
{
	$file=$_FILES['files'];
	$filename=$file['name'];
	$file_tmp=$file['tmp_name'];
	$file_size=$file['size'];
	$file_type=$file['type'];
	$file_ext=strtolower(end(explode('.',$filename)));
	$error=$file['error'];

	if($error==0){
		$file_dir="profileImage/";
		$file_destination=$file_dir.basename($filename);
		$filename_at_server=$filename;
		if(in_array($file_ext,$extension)){
			if(file_exists($file_destination)) {
				$count = 1;
				while(file_exists($file_dir.$count."_".basename($filename))){
					$count++;
				}
				$filename = $count."_".basename($filename);
			}
			$file_destination = $file_dir.$filename;

			if(move_uploaded_file($file_tmp, $file_destination)){
				$sql = "select picture from $tbl_name where id=$id;";
				$result = mysqli_query($link,$sql) or die ("A query error 1 has occured.");
				$row = mysqli_fetch_assoc($result);
				unlink($file_dir.$row["picture"]);
				$filename = mysqli_escape_string($link,stripcslashes($filename));
				$sql="UPDATE $tbl_name SET picture='$filename' WHERE id='$id'";
				$query = mysqli_query($link,$sql) or header("location: dashboard.php");
				header("location: dashboard.php");
			}
		}
		else{
			header('location: dashboard.php?picmsg=1&profile=1');
		}
	}
	else{
		header('location: dashboard.php?picmsg=2&profile=1');
	}
}
?>
