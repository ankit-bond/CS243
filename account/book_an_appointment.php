<?php
    include_once("../dbconnect.php");
    $sql4 = "select * from $tbl_name where type = 'doctor';";
    $find = mysqli_query($link, $sql4);

//    while ($row4 = mysqli_fetch_assoc($find)) {
//        echo '<option value="'.$row4['id'].'" data-tokens="'.$row4['fullname'].'">'.$row4['fullname'].'</option>';
//    }

?>
<div id="book-an-appointment">
    <div class="container col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Book your Appointment</h3>
            </div>
            <div class="container">
                <div class="panel-body">
                    <div class="form-group">
                        <label for="field" class="">Speciality</label>
                        <div class="">
                            <select class="selectpicker" data-live-search="true" id="field">
                                <option disabled>Select According to Speciality</option>
                                <?php
                                    $file = fopen("../specialities.txt", "r") or die("Unable to open file!");
                                    $text = fread($file,filesize("../specialities.txt"));
                                    fclose($file);
                                    $spec = explode("\n", $text);
                                    $select = '';
                                    $i = 0;
                                    foreach($spec as $option){
                                        $i++;
                                        $select .= '<option value="'.$i.'" data-tokens="'.$option.'">'.$option.'</option>
                                                ';
                                    }
                                    echo $select;
                                ?>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="submit" onclick="fetchfield()" class="btn btn-danger btn-toolbar"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;&nbsp;&nbsp;Search According To Speciality</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="searchbar" class="">Doctor</label>
                        <div class="">
                            <select class="selectpicker" data-live-search="true" id="searchbar">
                                <option disabled>Select A Doctor</option>
                                <?php
                                while ($row4 = mysqli_fetch_assoc($find)) {
                                    echo '<option value="'.$row4['id'].'" data-tokens="'.$row4['fullname'].'">Dr. '.ucfirst($row4['fullname']).'</option>';
                                }
                                ?>
                            </select>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="submit" onclick="fetch()" class="btn btn-warning btn-toolbar"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;&nbsp;&nbsp;Check Doctor's Profile</button>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div class="panel-footer">
                <p class="text-danger">
                    <?php if(isset($_GET['booked'])){
                        echo 'Sorry, you already have a pending appointment with doctor Dr. '.ucfirst($_GET['doctor']).'. You cannot book anymore appointments with him, until you attend or cancel your currently fixed appointment with him.' ;
                    } ?>
                </p>
            </div>

        </div>
        <div id="search_item">

        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(window).load(function () {
//        fetch();
    });

    $(document).ready(function(){
//        $( "#find" ).keyup(function(){
//            fetch();
        $('#book-slot').hide();
        });
    $(function() {
        $( "#radio" ).buttonset();
    });
//    function checker(){
//        console.log(document.getElementById('searchbar').value);
//    }
    function fetch()
    {
        xmlhttp=new XMLHttpRequest();
        var val = document.getElementById('searchbar').value;
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                 htmltext = xmlhttp.responseText;
                htmltext = htmltext.split('@@##');
                googleaddress = document.getElementById('googleaddress').innerHTML;
                joinedhtml = htmltext[0]+googleaddress+htmltext[1];
                document.getElementById("search_item").innerHTML=joinedhtml;

//                console.log(document.getElementById('searchbar').value);
            }
        };
        xmlhttp.open("GET","../appointment/search.php?get_val="+val,true);
        xmlhttp.send();
    }
    function fetchfield()
    {
        xmlhttp=new XMLHttpRequest();
        var val = document.getElementById('field').value;
        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                htmltext = xmlhttp.responseText;
                googleaddress = document.getElementById('googleaddress').innerHTML;
                htmltext = htmltext.replace(/@@##/g,googleaddress)
                document.getElementById("search_item").innerHTML=htmltext;
//                console.log(document.getElementById('searchbar').value);
            }
        };
        xmlhttp.open("GET","../appointment/searchfield.php?get_val="+val,true);
        xmlhttp.send();
    }
</script>
