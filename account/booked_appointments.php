<div id="booked-appointments">
    <h1 class="page-header">Appointments</h1>
    <div class="table-responsive">
    <table class="table-bordered table-responsive table-striped table">
        <?php
        $id = $_SESSION["id"];
        if ($row['type'] =='patient') {
            $client = 'pid';
        }
        else {
            $client = 'did';
        }

        $sql2 = "select * from $tbl_name2 where $client=$id ORDER BY valid DESC ;";
        $result2 = mysqli_query($link,$sql2);

        $count = mysqli_num_rows($result2);
        $k = 0;
        if($count >0 and $row['type'] =='patient'){
            echo "<tr><th>S.No.</th><th>Doctor</th><th>Timing</th><th>Status</th></tr>";
            while($row2 = mysqli_fetch_assoc($result2)){
                $valid = status($row2['valid']);
                $cancel = cancel_button($row2['valid'], $row2['id']);
                $did = $row2['did'];
                $sql3 = "select * from $tbl_name where id = $did;";
                $result3 = mysqli_query($link,$sql3);
                $row3 = mysqli_fetch_assoc($result3);
                $k++;
                echo "<tr><td>".$k."</td><td>Dr. ".ucfirst($row3['fullname'])."</td><td>".$row2["slot"]."</td><td>".$valid.'</td>'.$cancel.'</tr>';
            }
        }
        elseif($row['type'] =='patient'){
            echo '<h1>You currently have no booked appointments.</h1>
                    <p class="text-info">Book your appointments <a onclick="show_book_an_appointment()">here</a></p>';
        }
        elseif($count >0 and $row['type'] =='doctor'){
            echo "<tr><th>S.No.</th><th>Patient Name</th><th>Timing</th><th>Status</th></tr>";
            while($row2 = mysqli_fetch_assoc($result2)){
                $valid = status($row2['valid']);
                $cancel = cancel_button($row2['valid'], $row2['id']);
                $pid = $row2['pid'];
                $sql3 = "select * from $tbl_name where id = $pid;";
                $result3 = mysqli_query($link,$sql3);
                $row3 = mysqli_fetch_assoc($result3);
                $k++;
                echo "<tr><td>".$k."</td><td>Mr. ".ucfirst($row3['fullname'])."</td><td>".$row2["slot"]."</td><td>".$valid.'</td>'.$cancel.'</tr>';
            }
        }
        elseif($row['type'] =='Doctor'){
            echo '<h1>Sorry but there are no appointments booked with you yet.</h1>';
        }

        function status($valid) {
            if($valid==2){
                return 'Pending';
            }
            elseif($valid==0){
                return 'Canceled';
            }
            else{
                return 'Attended';
            }
        }

        function cancel_button($valid,$id){
            if($valid==2){
                return '<td><a href="../appointment/cancel.php?aid='.$id.'" class="btn btn-danger">Cancel</a></td>';
            }
            return '';
        }
        ?>
    </table>
    </div>

</div>