<?php
include("../dbconnect.php");
session_start();
if(!isset($_SESSION['username']) and !isset($_SESSION['id'])){
	header("location:../login/login_page.php");
}

$pid=$_SESSION['id'];
$extension=array("pdf","doc","docx","jpg","jpeg","png");
$flag=1;
if(isset($_FILES['files']))
{
	$file=$_FILES['files'];
	$filename=$file['name'];
	$file_tmp=$file['tmp_name'];
	$file_size=$file['size'];
	$file_ext=strtolower(end(explode('.',$filename)));
	$error=$file['error'];
	if($file_size==0){
		$msg="This much file size can 't be uploaded.Please select another file";
		$flag=0;
	}
	else{
		if($error==0){
		$file_dir="uploads/";
		$file_destination=$file_dir.basename($filename);
		$filename_at_server=$filename;

			if(in_array($file_ext,$extension)){
				if(file_exists($file_destination)) {
					$count = 1;
					while(file_exists($file_dir.$count."_".basename($filename))){
						$count++;
					}
					$filename = $count."_".basename($filename);
				}
				$file_destination = $file_dir.$filename;

				if(move_uploaded_file($file_tmp, $file_destination)){

					$filename = mysqli_escape_string($link,stripcslashes($filename));
					$sql="INSERT INTO $tbl_name5(filename,size,filetype,pid) VALUES('$filename','$file_size','$file_ext','$pid')";
					$query = mysqli_query($link,$sql) or header("location: dashboard.php");
					header('location: ../encryption/encrypt.php?file='.$file_destination);
				}
			}
			else{
				header('location: dashboard.php?msg=1&reports=1');
			}
		}
		else{
			header('location: dashboard.php?msg=2&reports=1');
		}
  }
}
?>