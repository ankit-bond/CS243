<div id="edit-appointment">
    <form action="../appointment/timings.php?id=<?php echo $id; ?>" method="post">
        <table class="table table-striped table-bordered table-responsive">
            <tbody>
            <tr>
                <th><h4>Day</h4></th><th><h4>Open/Closed</h4></th><th><h4>Opening Time</h4></th><th><h4>Closing Time</h4></th>
            </tr>

            <tr>

                <th>Monday</th>
                <td>
                    <select name="monday" class="selectpicker" >
                        <option <?php if($row3['monday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['monday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startmonday">
                            <input name="startmonday" class="form-control" type="text" value="<?php echo $row3['startmonday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closemonday">
                            <input name="closemonday" class="form-control" type="text" value="<?php echo $row3['closemonday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startmonday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closemonday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>

            </tr>
            <tr>
                <th>Tuesday</th>
                <td>
                    <select name="tuesday" class="selectpicker" >
                        <option <?php if($row3['tuesday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['tuesday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="starttuesday">
                            <input name="starttuesday" class="form-control" type="text" value="<?php echo $row3['starttuesday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closetuesday">
                            <input name="closetuesday" class="form-control" type="text" value="<?php echo $row3['closetuesday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#starttuesday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closetuesday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <th>Wednesday</th>
                <td>
                    <select name="wednesday" class="selectpicker" >
                        <option <?php if($row3['wednesday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['wednesday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>

                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startwednesday">
                            <input name="startwednesday" class="form-control" type="text" value="<?php echo $row3['startwednesday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closewednesday">
                            <input name="closewednesday" class="form-control" type="text" value="<?php echo $row3['closewednesday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startwednesday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closewednesday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <th>Thursday</th>
                <td>
                    <select name="thursday" class="selectpicker" >
                        <option <?php if($row3['thursday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['thursday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>

                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startthursday">
                            <input name="startthursday" class="form-control" type="text" value="<?php echo $row3['startthursday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closethursday">
                            <input name="closethursday" class="form-control" type="text" value="<?php echo $row3['closethursday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startthursday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closethursday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <th>Friday</th>
                <td>
                    <select name="friday" class="selectpicker" >
                        <option <?php if($row3['friday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['friday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>

                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startfriday">
                            <input name="startfriday" class="form-control" type="text" value="<?php echo $row3['startfriday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closefriday">
                            <input name="closefriday" class="form-control" type="text" value="<?php echo $row3['closefriday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startfriday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closefriday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <th>Saturday</th>
                <td>
                    <select name="saturday" class="selectpicker" >
                        <option <?php if($row3['saturday'] == 1) echo 'selected'; ?> value="1" >Open</option>
                        <option <?php if($row3['saturday'] == 0) echo 'selected'; ?> value="0" >Close</option>
                    </select>
                </td>

                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startsaturday">
                            <input name="startsaturday" class="form-control" type="text" value="<?php echo $row3['startsaturday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closesaturday">
                            <input name="closesaturday" class="form-control" type="text" value="<?php echo $row3['closesaturday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startsaturday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closesaturday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <th>Sunday</th>
                <td>
                    <select name="sunday" class="selectpicker">
                        <option <?php if($row3['sunday'] == 1) echo 'selected'; ?> value="1">Open</option>
                        <option <?php if($row3['sunday'] == 0) echo 'selected'; ?> value="0">Close</option>
                    </select>
                </td>

                <td>
                    <div class="form-group">
                        <div class='input-group date' id="startsunday">
                            <input name="startsunday" class="form-control" type="text" value="<?php echo $row3['startsunday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        <div class='input-group date' id="closesunday">
                            <input name="closesunday" class="form-control" type="text" value="<?php echo $row3['closesunday']; ?>">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-time"></span>
                    </span>
                        </div>
                    </div>
                </td>
                <script type="text/javascript">
                    $(function () {
                        $('#startsunday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                        $('#closesunday').datetimepicker({
                            language: 'us',
                            pickDate: false,
                            use24hours: false
                        });
                    });
                </script>
            </tr>
            <tr>
                <td></td>
                <td colspan="3">
                    <button id="savetime" type="submit" class="btn btn-block btn-danger"><strong>Save</strong></button>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
