<div id="view-patients">
    <?php
    $sql9 = "SELECT * FROM  $tbl_name2  WHERE did = $id AND ( valid =1 OR valid =2 ) ORDER BY valid DESC ;";
    $result9 = mysqli_query($link,$sql9);
    $count = mysqli_num_rows($result9);
    $pids = array();
    while ($row9=mysqli_fetch_array($result9)) {
        array_push($pids,$row9['pid']);
    }
    $pids=array_unique($pids);
    $k = 0;
    $html = '';
    $table ='';
    $modals = '';
    $script = '';

    if($count >0){
        foreach($pids as $pid) {
            $sql9 = "select * from $tbl_name where id = $pid;";
            $result9 = mysqli_query($link, $sql9);
            $row9 = mysqli_fetch_assoc($result9);
            $k++;
            $table .= tablehtml($row9,$k);
            $modals .= modalhtml($row9,$k);
            $script .= script($k);

        }
        $table = '<table class = "table-responsive table">'.$table.'</table>';
        $xml  = reschedule_appointment();
        $script = '<script>'.$script.'
        '.$xml.'</script>';

        echo $table.$modals.$script;
    }
    else {
        echo '<h1>You currently have no patients.</h1>';
    }

    function tablehtml($row,$index){
        return '<tr><td>'.$index.'</td><td>Mr. '.$row['fullname'].'</td><td>'.edit_button($index).'</td></tr>';
    }

    function modalhtml($row,$index) {
        $tbl_name2 = $GLOBALS['tbl_name2'];
        $link = $GLOBALS['link'];
        $id = $GLOBALS['id'];
        $pid = $row['id'];

        $sql9 = "SELECT * FROM  $tbl_name2  WHERE pid = $pid AND did= $id AND valid = 2;";
        $result9 = mysqli_query($link,$sql9);
        $count = mysqli_num_rows($result9);
        if($count==1){
            $row9 = mysqli_fetch_assoc($result9);
            $pending = $row9['slot'];
        }
        else{
            $pending = 'None';
        }

        $reschedule_button = '';
        if(check_rescheduling($id,$pid)){
            $reschedule_button = '<li id="list-'.$pid.'" class="list-group-item"><button onclick="reschedule_appointment('.$id.','.$pid.')" class="btn btn-xs btn-warning">Reschedule Appointment</button></li>';
        }



        $sql9 = "SELECT * FROM  $tbl_name2  WHERE pid = $pid AND did= $id AND valid = 1;";
        $result9 = mysqli_query($link,$sql9);
        $count = mysqli_num_rows($result9);
        if($count>0){
            $min = 999999999999999;
            while($row9 = mysqli_fetch_assoc($result9)) {
                $keywords = preg_split("/[ :-]+/", $row9['slot']);
                $num = '';
                for($i=4;$i>=0;$i--){
                    $num .= $keywords[$i];
                }
                if($num<$min){
                    $mindate = $row9['slot'];
                }
            }
        }
        else{
            $mindate = 'None';
        }



        $list = '
<ul class="list-group">
    <li class="list-group-item"><b>Name: </b>'.$row['fullname'].'</li>
    <li class="list-group-item"><b>Age: </b>'.$row['age'].'</li>
    <li class="list-group-item"><b>Gender: </b>'.$row['gender'].'</li>
    <li class="list-group-item"><b>Phone No.: </b>'.$row['phone_number'].'</li>
    <li class="list-group-item"><b>Email: </b>'.$row['email'].'</li>
    <li class="list-group-item"><b>Pending Appointment: </b>'.$pending.'</li>
    <li class="list-group-item"><b>Last Appointment: </b>'.$mindate.'</li>
    '.$reschedule_button.'
</ul>';
        $html =  '
<div class="modal fade" id="LoginModal-'.$index.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"'.$index.'>Patients Info</h4>
            </div>
            <div class="modal-body">
                '.$list.'
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>';
        return $html;
    }

    function script($index){
        return "
        $('#LoginModal-".$index."').on('shown.bs.modal', function () {
        $('#myInput').focus()
    });";
    }

    function edit_button($index){
            return '<button data-toggle="modal" data-target="#LoginModal-'.$index.'" class="btn btn-xs btn-danger">View Info</button>';
    }

    function check_rescheduling($did,$pid){
        $tbl_name2 = $GLOBALS['tbl_name2'];
        $link = $GLOBALS['link'];

        $sql = "SELECT * FROM  $tbl_name2  WHERE pid = $pid AND did= $did AND valid = 2;";
        $result = mysqli_query($link,$sql);
        $count = mysqli_num_rows($result);
        if($count>0){
            return 0;
        }
        return 1;

    }

    function reschedule_appointment(){
        $script = '
        function reschedule_appointment(did,pid)
    {
        xmlhttp=new XMLHttpRequest();

        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                console.log(xmlhttp.responseText+"========");
//                row.innerHTML=xmlhttp.responseText;
                document.getElementById("list-"+pid).innerHTML = xmlhttp.responseText;
            }
        };

        xmlhttp.open("GET","../appointment/reschedule_appointment.php?did="+did+"&pid="+pid,true);
        xmlhttp.send();
    }';
        return $script;
    }

    ?>

</div>