<div id="booked-appointments">
    <h1 class="page-header">Appointments</h1>
<!--    <div class="container">-->
        <table class="table-bordered table-responsive table-striped table table">
            <?php
            $id = $_SESSION["id"];
            $sql2 = "select * from $tbl_name2 where did = $id ORDER BY valid DESC ;";
            $result2 = mysqli_query($link,$sql2);

            $count = mysqli_num_rows($result2);
            $k = 0;

            if($count >0){
                echo "<tr><th>S.No.</th><th>Patient Name</th><th>Timing</th><th>Status</th></tr>";
                while($row2 = mysqli_fetch_assoc($result2)){
                    $k++;
                    $valid = status($row2['valid']);
                    $cancel = cancel_button($row2['valid'], $row2['id'],$k);
                    $pid = $row2['pid'];
                    $sql3 = "select * from $tbl_name where id = $pid;";
                    $result3 = mysqli_query($link,$sql3);
                    $row3 = mysqli_fetch_assoc($result3);
                    echo "<tr id=row-".$row2['id']."><td>".$k."</td><td>Mr. ".ucfirst($row3['fullname'])."</td><td>".$row2["slot"]."</td><td>".$valid.'</td>'.$cancel.'</tr>';
                }
            }
            else {
                echo '<h1>Sorry but there are no appointments booked with you yet.</h1>';
            }

            function status($valid) {
                if($valid==2){
                    return 'Pending';
                }
                elseif($valid==0){
                    return 'Canceled';
                }
                else{
                    return 'Attended';
                }
            }

            function cancel_button($valid,$id,$k){
                if($valid==2){
                    return '<td><a href="../appointment/cancel.php?aid='.$id.'" class="btn btn-xs btn-danger">Cancel</a>&nbsp;&nbsp;
                    <button onclick="edit_row('.$id.','.$k.')" class="btn btn-xs btn-warning">Reschedule Appointment</button>&nbsp;&nbsp;
                    <a href="../appointment/attended_appointment.php?aid='.$id.'&index='.$k.'" class="btn btn-xs btn-success">Appointment Over</a></td>';
                }
                return '';
            }
            ?>
        </table>
<!--    </div>-->
</div>
<script type="text/javascript">
    function edit_row(rowid,index)
    {
        xmlhttp=new XMLHttpRequest();

        xmlhttp.onreadystatechange=function() {
            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                console.log(xmlhttp.responseText+'========');
//                row.innerHTML=xmlhttp.responseText;
                document.getElementById("row-"+rowid).innerHTML = xmlhttp.responseText;
                document.getElementById("editit").innerHTML = xmlhttp.responseText;
            }
        };

        xmlhttp.open("GET","../appointment/edit_appointment_row.php?rowid="+rowid+'&index='+index,true);
        xmlhttp.send();
    }
</script>