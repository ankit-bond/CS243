<div id="profile" >
    <h1 class="page-header">Profile</h1>
    <div class="row placeholders">
        <div class=" placeholder">
            <img src="profileImage/<?php echo $row["picture"]; ?>" width="200px" height="200px" class="img-responsive" alt="profile">
            <h4><?php echo ucfirst($row["fullname"]); ?></h4>
            <span class="text-muted"><?php echo ucfirst($row["type"]); ?></span>
            <form action="pictureUpload.php" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="files">Update Profile photo</label>
                    <input type="file" name="files" id="files" class="col-xs-offset-5" required>
                    <p class="help-block">File size should be less than 2 MB.</p>
                </div>
                <div>
                    <button type="submit" class="btn btn-default" name="upload">Upload Image</button>
                </div>
            </form>
        </div>
        <?php
        if(isset($_GET['profile']))
        if($_GET['profile']==1){
            if(isset($_GET['picmsg'])){
                if($_GET['picmsg']==1){
                    echo '<p class="text-info">Only file types allowed: JPG, JPEG, PNG</p>';
                }
                elseif($_GET['picmsg']==2){
                    echo '<p class="text-info">File Size exceeded limit.</p>';
                }
            }
        }
        ?>
    </div>
    <h2 class="sub-header">Details</h2>
    <div class="row">
        <div class="panel panel-default">
            <!-- List group -->
            <div class="panel-heading">
            </div>
            <div class="body">
                <table class="table">
                    <tr>
                        <th>Username: </th>
                        <td><?php echo $row["username"]; ?></td>
                    </tr>
                    <tr>
                        <th>Email Id: </th>
                        <td><?php echo $row["email"]; ?></td>
                    </tr>
                    <tr>
                        <th>Name: </th>
                        <td><?php echo ucfirst($row["fullname"]); ?></td>
                    </tr>
                    <tr>
                        <th>Age: </th>
                        <td><?php echo $row["age"]; ?></td>
                    </tr>
                    <tr>
                        <th>Gender: </th>
                        <td><?php echo ucfirst($row["gender"]); ?></td>
                    </tr>
                    <tr>
                        <th>Phone Number: </th>
                        <td><?php echo $row["phone_number"]; ?></td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer" >
            </div>
        </div>
    </div>
</div>