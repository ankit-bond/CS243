<script>
        window.fbAsyncInit = function() {
                FB.init({
                appId: '181347822253454',
                status: true,
                cookie: true,
                xfbml: true
            });
        };
        (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
        }(document));
        function logout() {
            FB.logout(function(response) {
              PARS();
            });
        }
        var status = FB.getLoginStatus();
        function PARS() {
	    	FB.api('/me', function(response) {
	        var name=response.name;
	        document.location='../login/logout.php';

      });
  }
</script>
<?php
	include("../dbconnect.php");
	session_start();
	if(!isset($_SESSION['username']) and !isset($_SESSION['id'])){
		header("location:../login/login_page.php");
	}

	$username=$_SESSION["username"];
	$sql = "select * from $tbl_name where username = '$username'";
	$result = mysqli_query($link,$sql);
	$row = mysqli_fetch_assoc($result);
	$id=$row['id'];
	$name=$row['fullname'];
	$_SESSION['patient_id']=$id;
	$_SESSION['fullname']=$name;
	$type=$row['type'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard</title>


	<link href="../css/bootstrap.css" rel="stylesheet">
	<link href="../css/datepicker.min.css" rel="stylesheet">
	<link href="../css/bootstrap-select.min.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/moment.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap-select.min.js"></script>
	<script type="text/javascript" src="../js/en-gb.js"></script>
	<script type="text/javascript" src="../js/jquery-ui.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link href="../css/ie10-viewport-bug-workaround.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="../css/dashboard.css" rel="stylesheet">
	<link href="../css/style.css" rel="stylesheet">
	<script type="text/javascript" src="../js/style.js"></script>

	<!--[if lt IE 9]>
	<script src="../js/html5shiv.min.js"></script>
	<script src="../js/respond.min.js"></script>
	<![endif]-->

	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
	<script src="../js/holder.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="../js/ie10-viewport-bug-workaround.js"></script>

</head>

<body>

<nav class="navbar navbar-default navbar-fixed-top top-nav">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">PARS</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="nav-hover"><a href="">Home<span class="sr-only">(current)</span></a></li>
				<li class="nav-hover"><a href="../forum/forum_main.php">Discussion</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="nav-hover"><a href=""><?php echo "Welcome ".ucfirst($name);?></a></li>
				<?php if ($_SESSION['fb_login']!=1):?>
					<li class="nav-hover"><a href="../login/logout.php"><span class="glyphicon glyphicon-log-out"></span>&nbsp;&nbsp;Logout</a></li>
				<?php endif ;?>
				<?php if($_SESSION['fb_login']):?>
					<button onclick="javascript:logout();" class="btn btn-primary" style="margin-top:10px;" >Logout</button>
				<?php endif; ?>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class="container-fluid">
	<div class="col-sm-3 col-md-2 sidebar">
		<ul class="nav nav-sidebar">
			<li><a href="#" id="link-profile">Profile</a></li>
		</ul>
		<ul class="nav nav-sidebar">

			<?php
				if($row['type']=='patient')
					echo '<li><a href="#" id="link-reports">Reports</a></li>';
			?>
			<?php
			if($row['type']=='doctor')
				echo '<li><a href="#" id="link-view-patients">View Patients</a></li>';
			?>

<!--			<li><a href="#">Messages</a></li-->
			<li><a href="#" id="link-booked-appointments">
					<?php
					if($row['type']=='patient')
						echo 'Booked Appointments';
					else
						echo 'Manage Appointments'; ?>
				</a>
			</li>
			<?php
				if($row["type"] == "patient")
					echo "<li><a href=\"#\" id=\"link-book-an-appointment\">Book an Appointment</a></li>";
			?>
		</ul>
		<ul class="nav nav-sidebar">
			<?php
				if($row['type'] == 'doctor'){
					echo '<li><a href="#" id="link-set-appointment">Set Appointment Timings</a></li>';
				}
			?>
			<li><a href="#" id="link-update-info">Update Your Info</a></li>
		</ul>
	</div>

	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<?php 
			if($row['type']=="patient")
				include_once("map.php");
		?>
		<?php include_once("profile.php"); ?>
		<?php
			if($row["type"] == "patient"){
				include_once("book_an_appointment.php");
				include_once("booked_appointments.php");
				include_once("reports.php");
			}
		else{
			include_once("set_appointment.php");
			include_once("manage_appointments.php");
			include_once ("view_patients.php");
		}

		?>
		<?php include_once("edit.php"); ?>

	</div>



</div>
</body>
</html>
<script>
	$(document).ready(function(){
		document.cookie = 'googleaddress='+document.getElementById('googleaddress')+';';
		$('#googleaddress').hide();
		$('#profile').show();
		$('#booked-appointments').hide();
		$('#book-an-appointment').hide();
		$('#update-info').hide();
		$('#set-appointment').hide();
		$('#reports').hide();
		$('#view-patients').hide();
		$('ul.nav-sidebar li').removeClass('active');
		$('#link-profile').parent().addClass('active');
		$('#edit-appointment').hide();
		var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
				sURLVariables = sPageURL.split('&'),
				sParameterName,
				i;

			for (i = 0; i < sURLVariables.length; i++) {
				sParameterName = sURLVariables[i].split('=');

				if (sParameterName[0] === sParam) {
					return sParameterName[1] === undefined ? true : sParameterName[1];
				}
			}
		};
		if(getUrlParameter('set_appointment')==1){
			$('#profile').hide();
			$('#booked-appointments').hide();
			$('#book-an-appointment').hide();
			$('#update-info').hide();
			$('#set-appointment').show();
			$('#reports').hide();
			$('#view-patients').hide();
			$('ul.nav-sidebar li').removeClass('active');
			$('#link-set-appointment').parent().addClass('active');
		}
		else if (getUrlParameter('booked_appointments')==1)
		{

			$('#profile').hide();
			$('#booked-appointments').show();
			$('#book-an-appointment').hide();
			$('#update-info').hide();
			$('#reports').hide();
			$('#view-patients').hide();
			$('ul.nav-sidebar li').removeClass('active');
			$('#link-booked-appointments').parent().addClass('active');
		}
		else if (getUrlParameter('book_an_appointment')==1)
		{

			$('#profile').hide();
			$('#booked-appointments').hide();
			$('#book-an-appointment').show();
			$('#update-info').hide();
			$('#reports').hide();
			$('#view-patients').hide();
			$('ul.nav-sidebar li').removeClass('active');
			$('#link-book-an-appointment').parent().addClass('active');
		}
		else if(getUrlParameter(('reports'))==1)
		{

				$('#profile').hide();
				$('#booked-appointments').hide();
				$('#book-an-appointment').hide();
				$('#update-info').hide();
				$('#reports').show();
				$('#view-patients').hide();
				$('ul.nav-sidebar li').removeClass('active');
				$('#link-book-an-appointment').parent().addClass('active');
		}
	});
	function show_edit(){
		$('#edit-appointment').show();
		$('#set-appointment').hide();
	}
	function show_book_an_appointment(){
		$('#booked-appointments').hide();
		$('#book-an-appointment').show();
	}
//	function show_slots(){
//		$('#book-an-appointment').hide();
//		$('#set-appointment').hide();
//	}
</script>
