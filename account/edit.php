<div class="col-xs-offset-3" id="update-info">
	<h4 class="text-primary">Update Info</h4>
	<table class="table">
		<form method = "post" action = "update.php">
		<tr>
			<td>Username: </td>
			<td>
				<div class="form-group">
					<input class="form-control" type = "text" name = "username" value = "<?php echo $row["username"]; ?>">
				</div>
			</td>
		</tr>
		<tr>
			<td>Phone No.: </td>
			<td>
				<div class="form-group">
					<input class="form-control" type = "number" name = "phone_number" value = "<?php echo $row["phone_number"]; ?>">
				</div>
			</td>
		</tr>
		<tr>
			<td>Email Id: </td>
			<td>
				<div class="form-group">
					<input class="form-control" type = "email" name = "email" value = "<?php echo $row["email"]; ?>">
				</div>
			</td>
		</tr>
		<tr>
			<td>Age: </td>
			<td>
				<div class="form-group">
					<input class="form-control" type = "number" name = "age" value = "<?php echo $row["age"]; ?>">
				</div>
			</td>
		</tr>
		<tr>
			<td>
			<div class="radio">
				<label>
					<input  type="radio" name="gender" value="male" id="male" checked>
					Male&nbsp&nbsp
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="gender" value="female" id ="female">
					Female&nbsp&nbsp
				</label>
			</div>
			</td>
		</tr>
		<tr><td><input class="btn btn-block btn-primary" type = "submit" value = "Update"></td></tr>
		</form>
	</table>
	<br>
	<br>
</div>
