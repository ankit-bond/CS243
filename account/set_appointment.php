<?php
    $id = $row['id'];
    $sql3 = "select * from $tbl_name3 where id=$id;";
    $result3 = mysqli_query($link,$sql3);
    $row3 = mysqli_fetch_assoc($result3);
?>
<div id="set-appointment">
<table class="table table-striped table-bordered table-responsive">
    <tbody>
    <tr>
        <th><h4>Day</h4></th><th><h4>Open/Closed</h4></th><th><h4>Opening Time</h4></th><th><h4>Closing Time</h4></th>
    </tr>
    <tr>
        <th>Monday</th>
        <td><?php
                if($row3['monday'] == 1)
                    echo 'Open';
                else
                    echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startmonday']; ?></td>
        <td><?php echo $row3['closemonday']; ?></td>
    </tr>
    <tr>
        <th>Tuesday</th>
        <td><?php
            if($row3['tuesday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['starttuesday']; ?></td>
        <td><?php echo $row3['closetuesday']; ?></td>
    </tr>
    <tr>
        <th>Wednesday</th>
        <td><?php
            if($row3['wednesday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startwednesday']; ?></td>
        <td><?php echo $row3['closewednesday']; ?></td>
    </tr>
    <tr>
        <th>Thursday</th>
        <td><?php
            if($row3['thursday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startthursday']; ?></td>
        <td><?php echo $row3['closethursday']; ?></td>
    </tr>
    <tr>
        <th>Friday</th>
        <td><?php
            if($row3['friday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startfriday']; ?></td>
        <td><?php echo $row3['closefriday']; ?></td>
    </tr>
    <tr>
        <th>Saturday</th>
        <td><?php
            if($row3['saturday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startsaturday']; ?></td>
        <td><?php echo $row3['closesaturday']; ?></td>
    </tr>
    <tr>
        <th>Sunday</th>
        <td><?php
            if($row3['sunday'] == 1)
                echo 'Open';
            else
                echo 'Closed'; ?>
        </td>
        <td><?php echo $row3['startsunday']; ?></td>
        <td><?php echo $row3['closesunday']; ?></td>
    </tr>

    <tr>
        <td></td>
        <td colspan="3">
            <button id="changetime" onclick="show_edit()" type="submit" class="btn btn-block btn-warning"><strong>Change Timings</strong></button>
        </td>
    </tr>
    </tbody>
</table>
</div>
<?php include_once('edit_appointment.php'); ?>

