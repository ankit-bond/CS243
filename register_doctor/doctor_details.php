<?php
session_start();
if(isset($_SESSION['id']) and $_SESSION['username'])
{
    header('location: ../account/dashboard.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome to PARS</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/bootstrap-select.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/upload.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
    <script src="../js/bootstrap-select.min.js"></script>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
var geocoder = new google.maps.Geocoder();
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}
function updateMarkerPosition(latLng) {
  var latitude=latLng.lat();
  var longitude=latLng.lng();

    document.getElementById('longitude').innerHTML = '<input type="hidden" name="longitude" value="'+longitude+'">';

    document.getElementById('latitude').innerHTML = '<input type="hidden" name="latitude" value="'+latitude+'">';
}

function updateMarkerAddress(str) {
  document.getElementById('address').innerHTML = str;
  var address=str;
    document.getElementById('location').innerHTML = '<input type="hidden" name="address" value="'+address+'">';
}

function Mapshow() {
  var latLng = new google.maps.LatLng(26.316082,91.598396);
  var map = new google.maps.Map(document.getElementById('mapCanvas'), {
    zoom: 8,
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: 'Current location',
    map: map,
    draggable: true
  });
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  google.maps.event.addListener(marker, 'dragstart');
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });

  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
}
google.maps.event.addDomListener(window, 'load', Mapshow);
</script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="..">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="../forum/forum_main.php">Forum</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-hover"><a href="../login/main_login.php"><span class="glyphicon glyphicon-user"></span>&nbsp&nbspLogin</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container" >
    <div class="row" >
    
        <h1>
            Welcome <?php echo ucfirst($_POST['fullname']);?>
        </h1>
        <p class="text-info">
            We just a need few more details
        </p>
        <div class="col-xs-6">
            <form class="form" action="register_doctor.php" method="post">
                <br>
                <div class="form-group">
                    <label for="workplace">Hospital/Clinic Centre Name</label>
                    <input type="text" class="form-control" name="workplace" id="workplace" placeholder="Workplace">
                </div>
                <div class="form-group">
                    <label for="experience">Experience</label>
                    <input type="text" class="form-control" name="experience" id="experience" placeholder="Expereince">
                </div>
                <div class="form-group">
                    <label for="speciality">Speciality</label><br>
            <?php
            $file = fopen("../specialities.txt", "r") or die("Unable to open file!");
            $text = fread($file,filesize("../specialities.txt"));
            fclose($file);
            $spec = explode("\n", $text);
            $select = '';
            $i = 0;
            foreach($spec as $option){
                $i++;
                $select .= '<option value="'.$i.'" data-tokens="'.$option.'">'.$option.'</option>
';
            }
            $select = '<select name="speciality" id="speciality" class="selectpicker" data-live-search="true">
                '.$select.'
            </select>';
            echo $select;
            ?>
                </div>
                <div class="form-group">
                    <label for="extra">Any Extra Info</label>
                    <textarea id="extra" name="extra" class="form-control" rows="3"></textarea>
                </div>
                <?php
                    $data = '';
                    foreach ($_POST as $a => $b) {
                        $data.= "<input type='hidden' name='".htmlentities($a)."' value='".htmlentities($b)."'>
			            ";
                    }
                        echo $data;
                ?>
                <div id="location"></div>
                <div id="longitude"></div>
                <div id="latitude"></div>
                <div class="form-group">
                        <button type="submit" class="btn btn-success pull-left">Register</button>
                </div>
            </form>
            </div>
    <div class="col-xs-6" >
        <div id="mapCanvas"></div>
    </div>
    </div>
    </div>
<div style="margin-left:700px;" >
    <b>Matching address:</b>
<div id="address"></div>
</div>
</body
</html>