<?php
include_once("../dbconnect.php");
if(isset($_GET['get_val'])) {
    $speciality = $_GET['get_val'];
    $sql4 = "select * from $tbl_name where speciality = $speciality;";
    $find = mysqli_query($link, $sql4);
    if(mysqli_num_rows($find)>0) {

        while ($row4 = mysqli_fetch_assoc($find)) {
            if ($row4['picture'] == '') {
                $alt = "No image available";
            } else {
                $alt = $row4['fullname'];
            }
            if($row4['address']==''){
                $goodgleaddress = '<p>No address avialable.</p>';
            }
            else{
                $goodgleaddress = '<a id="googledirections" target="_blank" href="https://www.google.com/maps/dir/@@##/'.$row4['address'].'/" class="btn btn-default">Find Directions</a>';
            }
            $out = '
<div class="media">
	<div class="media-left">
		<img  style="border-radius: 10px; border: solid; border-color: #080808" class="media-object" width="200px" height="200px" src="' . $row4['picture'] . '" alt="' . $alt . '">
	</div>
	<div class="media-body">
		<ul class="list-group">
			<li class="list-group-item">
				<h4 class="media-heading"><span style="text-decoration: underline">Dr. ' . ucfirst($row4['fullname']) . '</span></h4>
			</li>
			<li class="list-group-item">
				<p><b>Experience:</b> ' . ucfirst($row4['experience']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Bio:</b> ' . ucfirst($row4['extra']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Address:</b> ' . ucfirst($row4['workplace']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Gender:</b> ' . ucfirst($row4['gender']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Age:</b> ' . ucfirst($row4['age']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Phone Number:</b> ' . ucfirst($row4['phone_number']) . '</p>
			</li>
			<li class="list-group-item">
				<p><b>Email ID:</b> ' . ucfirst($row4['email']) . '</p>
			</li>
			<li class="list-group-item">
				'.$goodgleaddress.'
			</li>
		</ul>
	</div>
</div>';
            echo '<div style="background-color: #f1f1f1; border-radius: 0px" class="panel panel-default"><div class="panel-body">'.$out;
            $uid = $row4['id'];
            $sql6 = "select * from $tbl_name3 where id =$uid;";
            $result6 = mysqli_query($link, $sql6);
            if(mysqli_num_rows($result6)==0){
                echo '<h4>Dr. '.ucfirst($row4['fullname']).' has not set his timetable yet.</h4><p class="text-info">You can call the doctor and ask him to set his appointments.</p>'.'</div></div>';
                continue;
            }
            $row6 = mysqli_fetch_assoc($result6);

            $count = 0;

            $mondayhtml = buildhtml('monday', 'Mon', $row6, $link, $tbl_name2, $uid, 'col-xs-offset-2');
            $tuesdayhtml = buildhtml('tuesday', 'Tue', $row6, $link, $tbl_name2, $uid);
            $wednesdayhtml = buildhtml('wednesday', 'Wed', $row6, $link, $tbl_name2, $uid);
            $thursdayhtml = buildhtml('thursday', 'Thurs', $row6, $link, $tbl_name2, $uid);
            $fridayhtml = buildhtml('friday', 'Fri', $row6, $link, $tbl_name2, $uid);
            $saturdayhtml = buildhtml('saturday', 'Sat', $row6, $link, $tbl_name2, $uid);
            $sundayhtml = buildhtml('sunday', 'Sun', $row6, $link, $tbl_name2, $uid);


            $slots = '<div id="book-slot">
    <div class="container col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Slots available with Dr. ' . ucfirst($row4['fullname']) . '</h3>
            </div>
            <form method="post" action="../appointment/appointment_book.php?did=' . $row4['id'] . '">
                <div class="panel-body">
                ' . $mondayhtml . $tuesdayhtml . $wednesdayhtml . $thursdayhtml . $fridayhtml . $saturdayhtml . $sundayhtml . '
                </div>
                <div class="panel-footer">
                <button type="submit" class="btn btn-danger btn-block"><span class="glyphicon glyphicon-bookmark"></span>&nbsp;&nbsp;&nbsp;&nbsp;Fix Appointment</button>
                </div>
            </form>
        </div>
    </div>
</div>';
            echo $slots.'</div></div>';

        }
    }
    else{
        echo '<p class="text-info">No doctors of this speciality were found.</p>';
    }
}

function buildhtml($day,$dayshort,$row6,$link,$tbl_name2,$id,$offset=''){
    if($row6[$day] == 1) {
        $keywords = preg_split("/[ :]+/", $row6['start'.$day]);
        $start = mktime($keywords[0],$keywords[1]);
        $start = date('H:i',$start);
        $keywords = preg_split("/[ :]+/", $row6['close'.$day]);
        $close = mktime($keywords[0],$keywords[1]);
        $close = date('H:i',$close);
        $daytime = $close - $start;
    }
    else {
        $daytime=0;
    }

    $dayhtml = '
                        <div id="question" class="col-xs-1 '.$offset.'" >
                                <b>'.date('d/m', strtotime("next ".$day, strtotime(date('l')))).' '.$dayshort.'</b>';
    for($i=0;$i<$daytime;$i++)
    {

        $value = date('H:i',mktime($start + $i,0)).' '.date('d-m-Y D', strtotime("next ".$day, strtotime(date('l'))));
        $sql7 = "select * from $tbl_name2 WHERE slot='$value' and did=$id";
        $result7 = mysqli_query($link,$sql7);
        $disable = '';
        if(availability($result7)){
            $disable = 'disabled';
        }

        $dayhtml .= '<input '.$disable.' type="radio" name="slot" id="radio-choice-'.$GLOBALS['count'].'" value="'.$value.'" required/>
                                <label for="radio-choice-'.$GLOBALS['count'].'" onclick="">'.date('H:i',mktime($start + $i,0)).'</label>';
        $GLOBALS['count']++;
    }
    $dayhtml .= '</div>';

    return $dayhtml;

}

function availability($result){
    $count = 0;
    while($row = mysqli_fetch_assoc($result)){
        if($row['valid']==2){
            $count++;
        }
    }
    if($count >=3){
        return 1;
    }
    return 0;
}
?>
