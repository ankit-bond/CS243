<?php
include_once("../dbconnect.php");
if(isset($_GET['did']) and isset($_GET['pid'])) {

    $did = $_GET['did'];
    $pid = $_GET['pid'];

    $sqla = "select * from $tbl_name3 where id =$did;";
    $resulta = mysqli_query($link, $sqla) or die(mysqli_error($link));
    $rowa = mysqli_fetch_assoc($resulta);

    $mondayhtml = buildhtml('monday','Mon',$rowa,$link,$tbl_name2,$did);
    $tuesdayhtml = buildhtml('tuesday','Tue',$rowa,$link,$tbl_name2,$did);
    $wednesdayhtml = buildhtml('wednesday','Wed',$rowa,$link,$tbl_name2,$did);
    $thursdayhtml = buildhtml('thursday','Thurs',$rowa,$link,$tbl_name2,$did);
    $fridayhtml = buildhtml('friday','Fri',$rowa,$link,$tbl_name2,$did);
    $saturdayhtml = buildhtml('saturday','Sat',$rowa,$link,$tbl_name2,$did);
    $sundayhtml = buildhtml('sunday','Sun',$rowa,$link,$tbl_name2,$did);


    $html = '
<form method="post" action="../appointment/reschedule_appointment_list.php?pid='.$pid.'" class="form-inline">
    <div class="form-group">
    <select class="form-control" name="slot">
        '.$mondayhtml.$tuesdayhtml.$wednesdayhtml.$thursdayhtml.$fridayhtml.$saturdayhtml.$sundayhtml.'
    </select>
    </div>
    <div class="form-group">
        <button class="btn btn-warning" type="submit">Fix Appointment</button>
    </div>
</form>';


    echo $html;

}

function buildhtml($day,$dayshort,$row6,$link,$tbl_name2,$id){
    if($row6[$day] == 1) {
        $keywords = preg_split("/[ :]+/", $row6['start'.$day]);
        $start = mktime($keywords[0],$keywords[1]);
        $start = date('H:i',$start);
        $keywords = preg_split("/[ :]+/", $row6['close'.$day]);
        $close = mktime($keywords[0],$keywords[1]);
        $close = date('H:i',$close);
        $daytime = $close - $start;
    }
    else {
        $daytime=0;
    }

    if($daytime>0){
        $dayhtml = '<p class="text-muted"><optgroup label="'.date('d/m', strtotime("next ".$day, strtotime(date('l')))).' '.ucfirst($day).'"></p>';

        for($i=0;$i<$daytime;$i++)
        {

            $value = date('H:i',mktime($start + $i,0)).' '.date('d-m-Y D', strtotime("next ".$day, strtotime(date('l'))));
            $sql7 = "select * from $tbl_name2 WHERE slot='$value' and did=$id";
            $result7 = mysqli_query($link,$sql7);
            $disable = '';
            if(availability($result7)){
                $disable = 'disabled';
            }
            $dayhtml .= '<option '.$disable.' value="'.$value.'">'.date('H:i',mktime($start + $i,0)).'</option>';
        }
        $dayhtml .= '</optgroup>';

        return $dayhtml;
    }
    return '';
}

function availability($result){
    $count = 0;
    while($row = mysqli_fetch_assoc($result)){
        if($row['valid']==2){
            $count++;
        }
    }
    if($count >=3){
        return 1;
    }
    return 0;
}

?>