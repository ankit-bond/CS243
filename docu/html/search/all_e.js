var searchData=
[
  ['read_5ffile',['read_file',['../namespacedecrypt.html#a0000f72dff04fa60565c8202896ed919',1,'decrypt.read_file()'],['../namespaceencrypt.html#afa47d39eb2a295555e4627a5340dd8b1',1,'encrypt.read_file()']]],
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['redirect_2ephp',['redirect.php',['../redirect_8php.html',1,'']]],
  ['register_5fdoctor_2ephp',['register_doctor.php',['../register__doctor_8php.html',1,'']]],
  ['registration_2ephp',['registration.php',['../registration_8php.html',1,'']]],
  ['registration_5fsuccess_2ephp',['registration_success.php',['../registration__success_8php.html',1,'']]],
  ['report_2ephp',['report.php',['../report_8php.html',1,'']]],
  ['reports_2ephp',['reports.php',['../reports_8php.html',1,'']]],
  ['reschedule_5fappointment',['reschedule_appointment',['../view__patients_8php.html#a02a65a19ce2dd009d78a8d8c5299df1e',1,'view_patients.php']]],
  ['reschedule_5fappointment_2ephp',['reschedule_appointment.php',['../reschedule__appointment_8php.html',1,'']]],
  ['reschedule_5fappointment_5flist_2ephp',['reschedule_appointment_list.php',['../reschedule__appointment__list_8php.html',1,'']]]
];
