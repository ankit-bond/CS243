var indexSectionsWithContent =
{
  0: "$abcdefghilmnprstuvw",
  1: "de",
  2: "abcdefghilmprstuv",
  3: "abcdemrstw",
  4: "$cefginpw",
  5: "pt"
};

var indexSectionNames =
{
  0: "all",
  1: "namespaces",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Namespaces",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

