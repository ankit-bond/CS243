<?php
session_start();
if(isset($_SESSION['id']) and $_SESSION['username'])
{
    header('location: account/dashboard.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Welcome to PARS</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/upload.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/style.js"></script>
    <script>
        window.fbAsyncInit = function() {
                FB.init({
                appId: '181347822253454',
                status: true,
                cookie: true,
                xfbml: true
            });
        };
        (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
        }(document));

        function login() {
            FB.login(function(response) {
                var userID=response.authResponse.userID;
                PARS(userID);
            }, {scope: 'public_profile,email'});            
        }
        var status = FB.getLoginStatus();
        function PARS(userID) {
        FB.api('/me', function(response) {
        var name=response.name;
        document.location='login/fb.php?id='+userID+'&name='+name;

      });
    }
    </script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="#">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="./forum/forum_main.php">Forum</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-hover"><a href="get.html">Get Started</a></li>
                <li class="dropdown nav-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp&nbsp<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li style="cursor: pointer;" data-toggle="modal" data-target="#LoginModal"><a href="login/login_page.php"><span class="glyphicon glyphicon-log-in"></span>&nbsp&nbspLogin</a></li>
                        <li><a href="register/main_registration.php"><span class="glyphicon glyphicon-user"></span>&nbsp&nbspRegister</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div>
<div style="position:fixed; z-index:-5;" >
    <iframe  width="1300px" height="550px" src="https://www.youtube.com/embed/IKx9wz_kR9M?autoplay=1&amp;showinfo=0&amp;controls=0&amp;loop=1&amp;"></iframe>
</div>
<div class="container col-lg-offset-3 col-md-offset-3 col-sm-12 col-lg-6 col-md-6" id="login-box">
    <div id="forum-links">
    <h3 style="color: white;">Top Three Discussions</h3>
    <?php 
        include("dbconnect.php");
        $sql="select * from forum";
        $result=mysqli_query($link,$sql);
        $i=1;
        while($row=mysqli_fetch_array($result) and $i<=3 ):?>
         <a style="width: 500px" class="row btn btn-default" href="forum/discussionpage.php?id=<?php echo $row['forum_id']; ?>">
            <?php echo $row['forum_name']; $i++;?>
         </a>
            <br>
    <?php endwhile;?>
</div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Log In</h3>
        </div>
        <form class="form form-horizontal" action="login/checklogin.php" method="POST">
            <div class="panel-body">
                <div class="form-group">
                    <label for="username" class="control-label col-md-4 col-sm-12">Username</label>
                    <div class="col-md-8 col-sm-12">
                        <input class="form-control" type="text" name="username" id="username" placeholder="Username" value="<?php if(isset($_GET['username'])){ echo $_GET['username'];} ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-md-4 col-sm-12">Password</label>
                    <div class="col-md-8 col-sm-12">
                        <input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
                        <p style="color:red"><?php
                            if(isset($_GET['wrong']))
                            echo "wrong username and password entered"; ?></p>
                    </div>
                </div>
                <button id="fb-btn" onclick="javascript:login();" class="btn btn-primary" style=" " >Login using Facebook</button>
                <br>
                <div class="col-md-8 col-lg-8 col-sm-12 pull-right" style="margin-top:10px;">
                    <button type="submit" class="btn btn-success btn-block pull-right"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;&nbsp;Log In</button>
                </div>
            </div>
        </form>

            <p class="text-info" style="margin-left:70px;" >
                <a href="register/main_registration.php">
                    <span class="glyphicon glyphicon-triangle-right pull-left"></span>
                    &nbsp;&nbsp;Sign Up Here
                </a>
            </p>
    </div>
</div>
</div>
</body>
</html>