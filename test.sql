-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 27, 2016 at 02:24 PM
-- Server version: 5.5.49-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot` text NOT NULL,
  `did` int(10) NOT NULL,
  `pid` int(10) NOT NULL,
  `valid` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` VALUES(15, '06:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(16, '06:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(17, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(18, '07:00 27-04-2016 Wed', 1, 2, 0);
INSERT INTO `appointments` VALUES(19, '06:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(20, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(21, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(22, '08:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(23, '07:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(24, '07:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(25, '09:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(26, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(27, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(28, '06:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(29, '08:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(30, '05:00 27-04-2016 Wed', 1, 2, 0);
INSERT INTO `appointments` VALUES(31, '05:00 27-04-2016 Wed', 1, 2, 0);
INSERT INTO `appointments` VALUES(32, '05:00 25-04-2016 Mon', 1, 2, 1);
INSERT INTO `appointments` VALUES(33, '07:00 26-04-2016 Tue', 1, 2, 1);
INSERT INTO `appointments` VALUES(34, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(35, '05:00 25-04-2016 Mon', 1, 2, 0);
INSERT INTO `appointments` VALUES(36, '08:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(37, '08:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(38, '09:00 26-04-2016 Tue', 1, 2, 0);
INSERT INTO `appointments` VALUES(39, '05:00 21-04-2016 Thu', 1, 2, 0);
INSERT INTO `appointments` VALUES(40, '07:00 21-04-2016 Thu', 1, 2, 0);
INSERT INTO `appointments` VALUES(41, '05:00 22-04-2016 Fri', 1, 2, 0);
INSERT INTO `appointments` VALUES(42, '14:00 27-04-2016 Wed', 1, 2, 1);
INSERT INTO `appointments` VALUES(43, '07:00 25-04-2016 Mon', 1, 14, 1);
INSERT INTO `appointments` VALUES(44, '07:00 28-04-2016 Thu', 1, 2, 1);
INSERT INTO `appointments` VALUES(45, '13:00 29-04-2016 Fri', 1, 2, 0);
INSERT INTO `appointments` VALUES(50, '06:00 28-04-2016 Thu', 1, 14, 0);
INSERT INTO `appointments` VALUES(51, '13:00 29-04-2016 Fri', 1, 2, 0);
INSERT INTO `appointments` VALUES(52, '12:00 27-04-2016 Wed', 1, 2, 2);
INSERT INTO `appointments` VALUES(53, '21:00 04-05-2016 Wed', 20, 21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `pid` int(10) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `filetype` varchar(100) NOT NULL,
  `size` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` VALUES(10, 2, 'index.jpeg', 'jpeg', 4887);

-- --------------------------------------------------------

--
-- Table structure for table `forum`
--

CREATE TABLE IF NOT EXISTS `forum` (
  `forum_id` int(4) NOT NULL AUTO_INCREMENT,
  `forum_name` varchar(70) NOT NULL,
  `forum_content` varchar(100) NOT NULL,
  `post_creator` varchar(70) DEFAULT NULL,
  `creator_email` varchar(70) DEFAULT NULL,
  `post_time` varchar(50) NOT NULL,
  `post_date` varchar(50) NOT NULL,
  PRIMARY KEY (`forum_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `forum`
--

INSERT INTO `forum` VALUES(1, 'Fever', 'I m having a fever.', 'Akash', 'akkidupare', '00:11:11', '0000-00-00');
INSERT INTO `forum` VALUES(6, 'Welcome', 'Welcome to the PARS Forum.', 'admin', '', '06:33:00', '0000-00-00');
INSERT INTO `forum` VALUES(7, 'What is Ebola exactly ??', 'Can i know something about ebola', 'ankit kumar', 'ankit@gmail.com', '01:58 pm', '27th  April');

-- --------------------------------------------------------

--
-- Table structure for table `forum_post`
--

CREATE TABLE IF NOT EXISTS `forum_post` (
  `post_id` int(4) NOT NULL AUTO_INCREMENT,
  `post_author` varchar(100) NOT NULL,
  `post_body` varchar(10000) NOT NULL,
  `forum_id` int(10) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `forum_post`
--

INSERT INTO `forum_post` VALUES(1, ' ', 'Hello Man', 1);
INSERT INTO `forum_post` VALUES(10, 'Akash', 'Yaa i m fine now', 1);
INSERT INTO `forum_post` VALUES(11, 'Akash', 'Thanks Doctor Aman!', 1);
INSERT INTO `forum_post` VALUES(12, 'arpan', 'Hello start your discusions here\r\n', 6);
INSERT INTO `forum_post` VALUES(13, 'vishal', 'nohing just feeling a bit dizzy ', 6);
INSERT INTO `forum_post` VALUES(14, 'Dr. akash', 'what are the symptoms exactly??', 6);
INSERT INTO `forum_post` VALUES(15, 'abhishek', 'i am good now', 1);
INSERT INTO `forum_post` VALUES(16, 'abhishek', 'Its a deadly disease currenlty rampant in africa.', 7);
INSERT INTO `forum_post` VALUES(17, 'abhishek', 'Its a deadly disease currenlty rampant in africa.', 7);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(70) NOT NULL,
  `username` varchar(70) NOT NULL,
  `password` varchar(70) NOT NULL,
  `fullname` varchar(70) NOT NULL,
  `phone_number` int(15) NOT NULL,
  `age` int(15) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `type` varchar(15) NOT NULL,
  `picture` text,
  `speciality` int(5) DEFAULT NULL,
  `experience` text,
  `extra` text,
  `workplace` text,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `address` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` VALUES(1, 'a@l.com', 'a', '0cc175b9c0f1b6a831c399e269772661', 'a', 12341423, 21, 'male', 'doctor', 'index.jpeg', 1, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(2, 'ba@b.baaa', 'b', '92eb5ffee6ae2fec3ad71c777531578f', 'b', 2147483647, 12, 'male', 'patient', '1_index.jpeg', NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(3, 'ab@ab.b', 'ab', '187ef4436122d1cc2f40dc2b92f0eba0', 'ab', 4, NULL, NULL, 'doctor', NULL, 1, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(4, 'ba@b.bb', 'ba', '07159c47ee1b19ae4fb9c40d480856c4', 'ba', 6, NULL, NULL, 'doctor', NULL, 3, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(5, 'asdd@a.cosok', 'asd', '7815696ecbf1c96e6894b779456d330e', 'asd', 2147483647, NULL, NULL, 'doctor', NULL, 2, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(8, 'asdd@a.cosok', 'asdas', '912ec803b2ce49e4a541068d495ab570', 'asd', 122311111, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(11, 'f@f.n', 'f', '0cc175b9c0f1b6a831c399e269772661', 'f', 555, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(12, 'abhiyadav1323@gmail.com', 'abhishek.cse', '0cc175b9c0f1b6a831c399e269772661', 'Abhishek', 2147483647, 19, 'male', 'doctor', '../uploads/P_20151130_195425_BF.jpg', 1, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(13, 'asd@asd.com', 'asd', '7815696ecbf1c96e6894b779456d330e', 'asd', 123456, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(14, 'asd@aaasd.com', 'asdf', '0cc175b9c0f1b6a831c399e269772661', 'asdf', 12345, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(16, 'arpanindora@iitg.ernet.in', 'arpan', 'f37427482b0311a8f3ea336ba0b60df1', 'arpan indora', 2147483647, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(17, 'vishal@gmail.com', 'vishal', '047b3072c7aba99daae34a347af74831', 'vishal kumar', 2147483647, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');
INSERT INTO `members` VALUES(18, 'ankit.2014@gmail.com', 'ankit', '447d2c8dc25efbc493788a322f1a00e7', 'ankit kumar', 1001510023, NULL, NULL, 'doctor', NULL, 1, '5 yrs', 'I am a good doctor.', 'IIT guwahati', 0, 0, '');
INSERT INTO `members` VALUES(19, 'vishal@high.com', 'vishal', '8b64d2451b7a8f3fd17390f88ea35917', 'vishal kumar', 2147483647, NULL, NULL, 'doctor', NULL, 1, '5 yrs', 'Experienced doctor with good reputation.', 'GNRC', 91.59839599999998, 26.316082, 'Unnamed Road, No.1 Bagta, Assam 781382, India');
INSERT INTO `members` VALUES(20, 'abhishelyadav@gmail.com', 'abhishek', 'f589a6959f3e04037eb2b3eb0ff726ac', 'ahishek yadav', 2147483647, 58, 'male', 'doctor', '2_index.jpeg', 1, '25 yrs', '', 'Dispur National Hospital', 91.59839599999998, 26.316082, 'Unnamed Road, No.1 Bagta, Assam 781382, India');
INSERT INTO `members` VALUES(21, 'vistar@iitg.ernet.in', 'vistaar', 'adaad2059344d91410b2e95e12fd4f33', 'vistaar juneja', 2147483647, NULL, NULL, 'patient', NULL, NULL, NULL, NULL, NULL, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `timetables`
--

CREATE TABLE IF NOT EXISTS `timetables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monday` tinyint(1) NOT NULL,
  `tuesday` tinyint(1) NOT NULL,
  `wednesday` tinyint(1) NOT NULL,
  `thursday` tinyint(1) NOT NULL,
  `friday` tinyint(1) NOT NULL,
  `saturday` tinyint(1) NOT NULL,
  `sunday` tinyint(1) NOT NULL,
  `startmonday` text,
  `starttuesday` text,
  `startwednesday` text,
  `startthursday` text,
  `startfriday` text,
  `startsaturday` text,
  `startsunday` text,
  `closemonday` text,
  `closetuesday` text,
  `closewednesday` text,
  `closethursday` text,
  `closefriday` text,
  `closesaturday` text,
  `closesunday` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `timetables`
--

INSERT INTO `timetables` VALUES(1, 1, 1, 1, 1, 1, 0, 0, '5:00 PM', '7:00 PM', '12:00', '5:00 PM', '09:46', '', '', '9:00 PM', '10:00 PM', '19:00', '9:00 PM', '18:55', '', '');
INSERT INTO `timetables` VALUES(20, 1, 1, 1, 1, 1, 0, 0, '09:00', '10:00', '08:00', '08:00', '10:00', '', '', '15:00', '18:00', '23:00', '19:00', '22:00', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
