<?php
    require 'db_connect.php';
    session_start();
    $user1=$_SESSION['username'];
    //$user2=$_SESSION['doctor'];
    //$user1="akash";
    $user2="aman";
?>
<html>
<head>
	<title>Chat Box</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/chatstyle.css" rel="stylesheet">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/style.js"></script>
    <script src="js/chat.js"></script>


</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="#">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="./forum/forum_main.php">Forum</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-hover"><a href="#">Get Started</a></li>
                <li class="dropdown nav-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp&nbsp<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li style="cursor: pointer;" data-toggle="modal" data-target="#LoginModal"><a><span class="glyphicon glyphicon-log-in"></span>&nbsp&nbspLogin</a></li>
                        <li><a href="register/main_registration.php"><span class="glyphicon glyphicon-user"></span>&nbsp&nbspRegister</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div>
    <?php
        include_once("login/main_login.php");
    ?>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
        </div>
        <div class="col-sm-6">
            <div class="chatContainer">
                <div class="chatHeader">
                    <h3><?php echo $user2; ?> </h3>
                </div>
                <div class="chatMessages"></div>
                <div class="chatBottom">
                    <form action="#" onSubmit='return false;' id="chatForm">
                        <input type="hidden" id="sender" value="<?php echo $user1; ?>">
                        <input type="hidden" id="receiver" value="<?php echo $user2; ?>">
                        <input type="text" name="text" id="text" placeholder="Type Your Message">
                        <input type="submit" name="submit" value="Post">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
        </div>
    </div>
</div>
</body>
</html>