# Patient Appointment Reservation System(PARS)

Doctor’s appointment management system is prepared for health sector medical
practitioners. It is also an immediate need and geo-location based medical aid discovery system with:

  - Integrated Appointment Reservation System 
  - Report and Prescription Delivery System
  - Secure Health Record Cloud Database
  - Online Counselling

The Project is to improve the quality Health Care Services,

> Increasing the availability, expedition and effectiveness of the information of the patient and the health care industry using the Web Based System that will serve as a bridge between the doctors and the patients. 
> This will digitize the OPD registration and appointment system through Hospital Management System. Make an online operating database for medical records, to make it available to the concerned doctor based on patient’s granted permission level.

### Technologies

Open source projects used:

* [PHP] - Server-side scripting language
* [Bootstrap] -  Front-end framework
* [MySql] - Database management system
* [Jquery] - JavaScript Library
* [Ajax] - Asynchronous JavaScript and XML
* [Python] - Encryption


### Modules

Modules implemented in the Web Application-

* ***Authentication Module:*** Logging and Registering users(doctor or patient).
* ***Appointment Module:*** Displays information about the available timing slots with a doctor.
* ***Medical Record Module:*** Secured private database for all the previous lab reports,
prescription and treatments taken by every patient.
* ***Q&A Module:*** Portal for a doctor and patients to interact.

License
----
MIT

Team Members
----
* Ankit Kumar
* Vishal Kumar
* Akash Dupare
* Arpan Indora


   [Bootstrap]: <http://getbootstrap.com/>
   [PHP]: <http://php.net/>
   [jQuery]: <http://jquery.com>
   [MySql]: <https://www.mysql.com/>
   [Ajax]: <www.asp.net/ajax>
   [Python]: <https://www.python.org> 