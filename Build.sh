#!usr/bin/bash

#PARS
#This script is to be run by developers only

echo "Updating your server"
echo
sudo apt-get update
echo

echo "Upgrading your server"
echo
sudo apt-get upgrade
echo

echo "Instaliing Apache server"
echo
sudo apt-get install apache2
echo

echo "Installing MySQL server"
echo
sudo apt-get install mysql-server php5-mysql
echo

echo "Installing php"
echo
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
echo

echo "Installing phpmyadmin"
echo
sudo apt-get install phpmyadmin
echo

echo "Restarting Apache"
echo
sudo service apache2 restart
echo

echo "Installing PyCrypto"
echo
sudo apt-get install python-crypto
sudo pip install simple-crypt
echo

echo "Setting up the Database"
echo
mysql -u root -p < test.sql
echo

echo "Giving the required permissions"
echo
chmod +x encryption/encrypt
chmod +x encryption/decrypt
echo
