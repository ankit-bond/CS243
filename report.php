<!DOCTYPE html>
<html>
<head>
  <title>Report</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./css/login.css">
</head>
<body>
    <div class="container">
        <div class="col-xs-12">
         <div class="col-sm-6 col-sm-offset-3">
            <h3 class="text-center">Uploaded Documents</a></h3>
          	<table class="table-responsive table table-bordered table-hover">
              <thead>
              <tr>
              <td>File Name</td>
              <td>File Type</td>
              <td>File Size (KB)</td>
              <td>View File</td>
              </tr>
            </thead>
            <tbody>
              <?php
          	$sql="SELECT * FROM table01";
          	$result_set=mysql_query($sql);
          	while($row=mysql_fetch_array($result_set))
          	{
          		?>
                  <tr>
                  <td><?php echo $row['filename'] ?></td>
                  <td><?php echo $row['filetype'] ?></td>
                  <td><?php echo (int) ($row['size']/1024) ?></td>
                  <td><a href="files/<?php echo $row['filename'] ?>" target="_blank">view</a></td>
                  </tr>
                  <?php
          	}
          	?>
            </table>
          </tbody>
          	<label><a href="upload.html">Upload new files...</a></label>
    
          </div>                    
        </div>
      </div>
    </div>
</body>
</html>