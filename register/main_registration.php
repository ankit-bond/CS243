<?php
session_start();
if(isset($_SESSION['id']) and $_SESSION['username'])
{
    header('location: ../account/dashboard.php');
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="../">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="../forum/forum_main.php">Forums</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-hover"><a href="#">Get Started</a></li>
                <li class="nav-hover">
                    <a href="../login/login_page.php">&nbsp&nbsp<span class="glyphicon glyphicon-user"></span>&nbsp&nbsp</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<div class="container col-lg-offset-3 col-md-offset-2 col-sm-12 col-lg-6 col-md-8">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Register</h3>
        </div>
        <form class="form-horizontal" action="registration.php" method="POST">
        <div class="panel-body">
                <div class="form-group">
                    <label for="fullname" class="control-label col-md-4 col-sm-12">Full Name</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="text" name="fullname" id="fullname" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="username" class="control-label col-md-4 col-sm-12">Username</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="text" name="username" id="username" placeholder="Username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-md-4 col-sm-12">Email Address</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="email" name="email" id="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-md-4 col-sm-12">Password</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="password" name="password" id="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirm-password" class="control-label col-md-4 col-sm-12">Confirm Password</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="password" name="confirm_password" id="confirm-password" placeholder="Confirm Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone-number" class="control-label col-md-4 col-sm-12">Phone Number</label>
                    <div class="col-md-8 col-sm-12">
                    <input class="form-control" type="number" name="phone_number" id="phone-number" placeholder="Mobile Number" required>
                    </div>
                </div>
                <div align="center">
                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="type" value="patient" checked>Patient
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" name="type" value="doctor">Doctor
                        </label>
                    </div>
                </div>
            <br>
            <div class="col-md-8 col-lg-8 col-sm-12 pull-right">
                <button type="submit" class="btn btn-success btn-block pull-right"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;&nbsp;&nbsp;Sign Up</button>
            </div>
            </div>
            <p class="text-info">
                <a href="../login/login_page.php">
                    <span class="glyphicon glyphicon-triangle-right pull-left"></span>
                    &nbsp;&nbsp;Log In Here
                </a>
            </p>
    </div>
    </form>
</div>

</body>
</html>