<?php
session_start();
if(isset($_SESSION['id']) and $_SESSION['username'])
{
    header('location: ../account/dashboard.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <link href="../css/bootstrap.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/style.js"></script>
</head>
<body>
<nav class="navbar navbar-default top-nav">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PARS</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="nav-hover"><a href="../">Home<span class="sr-only">(current)</span></a></li>
                <li class="nav-hover"><a href="../forum/forum_main.php">Forums</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-hover"><a href="#">Get Started</a></li>
                <li class="nav-hover">
                    <a href="../login/login_page.php">&nbsp&nbsp<span class="glyphicon glyphicon-user"></span>&nbsp&nbsp</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container col-lg-offset-3 col-md-offset-2 col-sm-12 col-lg-6 col-md-8">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="container text-success">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;&nbsp;&nbsp;
                <b>Registration Successful</b>
            </div>
        </div>
        <div class="panel-body">
            Your account has been registered. Now you can
            <a href="../login/login_page.php">Login Here.</a>
        </div>

    </div>
</div>

</body>
</html>