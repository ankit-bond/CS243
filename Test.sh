#!/bin/bash
# Testing using cURL

echo "Testing for login"
echo

echo "username = b and password = b"
curl --data "username=b&password=b" http://localhost/CS243/login/checklogin.php --dump-header output.txt
echo
cat output.txt
echo

echo "username = anki and password = banki"
curl --data "username=anki&password=banki" http://localhost/CS243/login/checklogin.php --dump-header output.txt
echo
cat output.txt
echo

echo "username = a  and password = a"
curl --data "username=a&password=a" http://localhost/CS243/login/checklogin.php --dump-header output.txt
echo
cat output.txt
echo

echo "username = a and password = abc"
curl --data "username=a&password=abc" http://localhost/CS243/login/checklogin.php --dump-header output.txt
echo
cat output.txt
echo

echo "Testing for Registration for Patients"
echo

echo "fullname=a username=anki email=a@d.com password=anki confirm_password=anki phone_number=294 type=patient"
curl --data "fullname=a&username=anki&email=a@d.com&password=anki&confirm_password=anki&phone_number=294&type=patient" http://localhost/CS243/register/registration.php --dump-header output.txt
echo
cat output.txt
echo

echo "fullname=ba username=banki email=ba@d.com password=banki confirm_password=banki phone_number=0294 type=patient"
curl --data "fullname=ba&username=banki&email=ba@d.com&password=banki&confirm_password=banki&phone_number=0294&type=patient" http://localhost/CS243/register/registration.php --dump-header output.txt
echo
cat output.txt
echo

echo "Testing for Registration for Doctors"
echo

echo "fullname=ca username=canki email=ca@d.com password=canki confirm_password=canki phone_number=1294 type=doctor"
curl --data "fullname=ca&username=canki&email=ca@d.com&password=canki&confirm_password=canki&phone_number=1294&type=doctor" http://localhost/CS243/register_doctor/register_doctor.php --dump-header output.txt
echo
cat output.txt
echo

echo "fullname=aba username=abanki email=aba@d.com password=abanki confirm_password=abanki phone_number=10294 type=doctor"
curl --data "fullname=aba&username=abanki&email=aba@d.com&password=abanki&confirm_password=abanki&phone_number=10294&type=doctor" http://localhost/CS243/register_doctor/register_doctor.php --dump-header output.txt
echo
cat output.txt
echo